// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://*/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function () {
  "use strict";

  const currencies = [
    "ALL",
    "ARS",
    "AZN",
    "BGN",
    "BRL",
    "BYN",
    "USD",
    "ZMW",
    "AED",
    "ISK",
  ];

  const modal = document.getElementById('exampleModalLive');
  const footer = modal.getElementsByClassName('modal-footer')[0];

  // create inline button container
  const buttonContainer = document.createElement('div');
  buttonContainer.className = 'btn-group';
  footer.prepend(buttonContainer);


  // add from and to dropdown fields to navbar
  const from = document.createElement("select");
  from.id = "from";
  from.className = "form-control";
  from.placeholder = "from";
  buttonContainer.appendChild(from);

  const to = document.createElement("select");
  to.id = "to";
  to.className = "form-control";
  to.placeholder = "to";
  buttonContainer.appendChild(to);

  for (let i = 0; i < currencies.length; i++) {
    const option = document.createElement("option");
    option.value = currencies[i];
    option.innerHTML = currencies[i];
    from.appendChild(option);
    to.appendChild(option.cloneNode(true));
  }

  from.selectedIndex = currencies.indexOf("CZK");
  to.selectedIndex = currencies.indexOf("TRY");

  const button = document.createElement("button");
  button.className = "btn btn-default";
  button.innerHTML = "Change";
  button.onclick = () => {
    const content = modal.getElementsByClassName('modal-body')[0]
    const to = $("#to")[0].value;
    const from = $("#from")[0].value;
    const re = new RegExp(from, "g");
    content.html(content.html().replace(re, to));
  };

  buttonContainer.appendChild(button);
})();
