// ==UserScript==
// @name         INK Scripts
// @version      0.1.5
// @description  QOL Scripts for INK
// @author       Bogurd
// @match        https://www.fundist.org/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// @require      https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js
// @require      https://cdn.jsdelivr.net/npm/file-saver@2.0.2/dist/FileSaver.min.js
// ==/UserScript==

(function () {
  "use strict";

  const currencies = [
    "ALL",
    "ARS",
    "AZN",
    "BGN",
    "BRL",
    "BYN",
    "CAD",
    "CHF",
    "CZK",
    "DKK",
    "EUR",
    "GEL",
    "HKD",
    "HRK",
    "IDR",
    "ILS",
    "LBP",
    "MAD",
    "MDL",
    "NOK",
    "PEN",
    "PLN",
    "RON",
    "RUB",
    "SEK",
    "TND",
    "TRY",
    "USD",
    "ZMW",
    "AED",
    "ISK",
  ];

  const modal = $("#gamedetails");
  const footer = modal.find(".modal-footer")[0];

  if (modal) {
    // const gameinfo = modal
    //   .find(".evo-details-table .evo-details-border")
    //   .find("th")[0].textContent;

    if (footer) {
      // create inline button container
      footer.className = "btn-group";
      footer.style.display = "flex";
      footer.style.padding = "3px";
      footer.style.flexDirection = "row";
      footer.style.justifyContent = "flex-end";
      
      const buttonContainer = document.createElement("div");
      buttonContainer.className = "btn-group";
      buttonContainer.style.display = "flex";
      buttonContainer.style.flexDirection = "row";
      buttonContainer.style.marginRight = "10px";

      footer.prepend(buttonContainer);

      // create button to take a screenshot of the game details
      const screenshotButton = document.createElement("button");
      screenshotButton.className = "btn btn-primary";
      screenshotButton.innerHTML = "Screenshot";
      screenshotButton.style.marginRight = "3px";
      screenshotButton.id = "screenshot-button";

      screenshotButton.onclick = function () {
        html2canvas($(".modal-body"), {
          onrendered: function (canvas) {
            canvas.toBlob(function (blob) {
              saveAs(blob);
            });
          },
        });
      };

      buttonContainer.appendChild(screenshotButton);

      $(function () {
        $("#screenshot-button").click(function () {});
      });

      // add from and to dropdown fields to navbar
      const from = document.createElement("select");
      from.id = "from";
      from.className = "form-control";
      from.style.maxWidth = "200px";
      from.placeholder = "from";
      from.style.marginRight = "3px";
      buttonContainer.appendChild(from);

      const to = document.createElement("select");
      to.id = "to";
      to.className = "form-control";
      to.style.maxWidth = "200px";
      to.placeholder = "to";
      to.style.marginRight = "3px";
      buttonContainer.appendChild(to);

      for (let i = 0; i < currencies.length; i++) {
        const option = document.createElement("option");
        option.value = currencies[i];
        option.innerHTML = currencies[i];
        from.appendChild(option);
        to.appendChild(option.cloneNode(true));
      }

      from.selectedIndex = currencies.indexOf("CZK");
      to.selectedIndex = currencies.indexOf("TRY");

      const changeCurrencyButton = document.createElement("button");
      changeCurrencyButton.className = "btn btn-default";
      changeCurrencyButton.innerHTML = "Convert";
      changeCurrencyButton.style.marginRight = "3px";
      changeCurrencyButton.onclick = () => {
        const content = modal.find(".modal-body");
        const to = $("#to")[0].value;
        const from = $("#from")[0].value;
        const re = new RegExp(from, "g");
        content.html(content.html().replace(re, to));
      };
      buttonContainer.appendChild(changeCurrencyButton);
    }
  }
})();
